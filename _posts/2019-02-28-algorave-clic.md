---
layout: post
categories: posts
title: "1er Algorave La Plata - TOPLAP 15"
---

# Algorave La Plata - TOPLAP 15 - CLiC

El 15/2/19 sucedió la primer [algorave](https://algorave.com/) de La Plata: esto es, fue y será un montón.

_Algorave, rave algoritmica: la unión de "rave" y "algoritmos", podría ser el acto de programar en vivo frente a una audiencia, como un desafío artístico y performance del cuerpo; con la posibilidad que el público "baile" frente a las imagenes y sonidos generados._

_Programar en vivo, live coding, podría ser la actividad de escribir (partes de) un programa mientras se ejecuta. De este modo, la práctica de programar en vivo conecta profundamente la causalidad algorítmica con el resultado percibido y, al deconstruir la idea de la dicotomía temporal entre la herramienta y el producto, permite que el código se ponga en juego como un proceso artístico. La naturaleza de estos algoritmos generados en ejecución es que se modifican en tiempo real por la/s personas que escriben el codigo._

Empezamos a producir el evento con meses de anticipación, [cuando Yaxu publicó](https://talk.lurk.org/channel/livecode?msg=kJgER75jkfvZaxgx5) que iba a hacerse un streaming global de 86 horas para el [cumpleaños 15 de TOPLAP](https://colectivo-de-livecoders.gitlab.io/blog/posts/2019/01/31/cumple-toplap.html) allá por el 1 diciembre de 2018.

La metodología serían "slots" de 30 minutos abiertos a toda persona (en una planilla online cualquiera podía poner su nombre y asi reservar su espacio); aunque primero abrieron "la inscripción" a organizaciónes de eventos en vez de a personas individuales.

Ahí reservamos 8 slots, porque no? hacer algo de 4 horas de duración, con personas de capital y la comunidad de la plata. Sin saber aún el lugar físico ni las participantes... eso vendría después. Pero la fecha ya estaba pautada: viernes 15 de febrero de 20:30 a 00:30

Y el lugar apareció: la UDE. Luego, unas semanas antes de la fecha, surgió la pregunta: que tal si le damos marco de algorave? Hasta el momento sólo contemplabamos ser parte del streaming.

Asi, auto-convocadxs y auto-organizadxs, nos pusimos en la organización de la algorave. Abrimos un [pad](https://pad.riseup.net/) (documento colaborativo hecho con herramientas de software libre) para que las personas que quisieran participar se anotaran. Y quedó este lineup:

* umachinita + carla + cancan

* kern3ll.Pan1c! + malabia

* vlad + lauty + manoloide

* munshkr + manoloide

* c0d3 p03try (rapo + flor de fuego)

* torotumbo + librenauta

* oblinof + flor de fuego

* CLiC


Y el resultado fue mejor de lo esperado: se pudieron ver y escuchar propuestas diversas, maneras particulares de pensar y ejecutar, estilos y modos de construír y escribir los algo-ritmos muy interesantes. A su vez fue un gran momento para conocernos personalmente, ya que gran parte de la actividad del grupo se realiza de manera virtual.

Video [aqui](https://youtu.be/q87tR60XPGE?t=12635)

Nos seduce eso de poder modificar la estructura y el programa desde el programa mismo, y en ejecución.

Pero no nos importa lenguaje, sistema operativo, género musical, estilo visual, whatever, que se use y/o genere. No hay opciones mejores que otras.
Tampoco nos interesa delimitar qué es y qué no es livecoding. Quiénes somos, acaso, para delinear tal nimiedad?

Sólo el resultado de la combinación de elementos que suceden en el mismo momento: esa es la impronta livecoding que nos interesa.
La improvisación musical/visual desde lenguajes de programación, ideas expresadas en texto colaborativamente.
