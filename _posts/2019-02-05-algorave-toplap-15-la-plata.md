---
layout: post
categories: posts
title: "ALGORAVE - TOPLAP 15 - La Plata"
---

# ALGORAVE - TOPLAP 15 - La Plata

![flyer]({{ site.baseurl }}/assets/img/algorave_clic.jpg){:class="img-responsive"}

El viernes 15/2/19 en el [marco del cumpleaños 15 de TOPLAP](https://colectivo-de-livecoders.gitlab.io/blog/posts/2019/01/31/cumple-toplap.html) nos autoconvocamos y organizamos para hacer la primer [algorave](https://algorave.com/) de La Plata en la [UDE (2 n684)](https://www.google.com/maps/place/Universidad+del+Este/@-34.9072749,-57.9504423,17z/data=!4m5!3m4!1s0x95a2e64797927369:0xe6adc50bfe9db079!8m2!3d-34.9072749!4d-57.9482536).
Participamos personas de CABA y La Plata.

El 8/2/2018, hace aproximadamente 1 año, fue la primer [algorave de Argentina](https://livecodear.github.io/documents/2018/02/15/algorave-1.html)

Esta vez será un streaming global, empezamos a las 20.30hs hasta las 00.30hs hora local. [link para ver el streaming](https://www.youtube.com/eulerroom/live/)

El lineup de livecoders:

* kern3ll.Pan1c!
* carla
* umachinita
* iris
* vlad
* lautyrainbow
* manoloide
* librenauta
* munshkr
* c0d3 p03try (rapo + flordefuego)
* torotumbo
* oblinof
* cancan
* malabia
* CLiC

Nos vemos ahi o/

CLiC
