---
layout: post
categories: posts
title: "Herramientas libres online en pandemia"
---

# Herramientas libres online en pandemia

El siguiente artículo es una colaboración y un impulso de un pad creado y difundido por [poemproducer](https://poemproducer.com/), artiste finlandese, con foco en el software libre y tecnologías p2p. Lo que figura es una copia del pad tal cual está al momento de esta publicación, traducida al español para la comunidad latinoamericana. La idea es conservar (al menos) esta información sin importar qué suceda con el pad. Para ver versiones más actualizadas del mismo cliquear [aquí](https://etherpad.wikimedia.org/p/online-tools-for-the-pandemic). **NOTA:** el contenido está mayormente en inglés; de todos modos siguen siendo fuentes de información valiosa que servirían para pensar nuestra realidad o replicar ideas.

## Open Source / FLOSS TOOLS - alternativas al software capitalista de vigilancia para comunicarnos

### Pensando críticamente acerca de las opciones que tenemos en las plataformas y tecnologías que usamos

Listas similares muy útiles 
- [https://libreplanet.org/wiki/Remote_Communication](https://libreplanet.org/wiki/Remote_Communication)
- [https://prism-break.org/en/](https://prism-break.org/en/)
- [https://ethical.net/](https://ethical.net/)
- [MANIFIESTO DEL FEMINISMO EN INTERNET](https://feministinternet.com/manifesto/)
- [Principios Feministas de Internet](https://feministinternet.org/ ) 

#### Dado que la lista es bastante larga, acá hay algunas categorías a las que podrás scrollear:

1. Conferencias audio-visuales
2. Programación de reuniones grupales
3. Streaming
4. Video hosting
5. Chat
6. Etherpads
7. Internet feminista
8. Redes sociales alternativas
9. Colaboración artística
10. Tecnología de audio
11. Desarrollo de videojuegos
12. Realidad virtual
13. Almacenamiento propio (con otres)
14. Teléfonos celulares
15. Para niñes
16. Seguridad en fotos
17. Links útiles
18. Compartir documentos
19. Solidaridad Digital
20. Artículos
21. Email
22. Dinero
23. Otros / sin categoría

## 1. CONFERENCIAS AUDIOVISUALES

- [Jitsi Meet](https://jitsi.org/jitsi-meet )  gratis, sin límite de participantes, basado en navegador (lo que significa que no hay nada que instalar para ningún participante) nueva versión mejorada con respecto al soporte de FF, e2ee y número de participantes. [Información sobre Jitsi](https://hub.libranet.de/articles/and-priv-sec/jitsi/) [Cómo auto-almacenar Ubuntu-Debian](https://guides.lw1.at/books/how-to-install-jitsi-meet-on-debian-or-ubuntu/) [instalación oficial](https://meet.jit.si/)  (también la última versión estable)
otras instancias de varias organizaciones: [https://meet.mayfirst.org](https://meet.mayfirst.org) / [https://calls.disroot.org](https://calls.disroot.org) / [https://vc.autistici.org](https://vc.autistici.org) / [https://framatalk.grifon.fr](https://framatalk.grifon.fr)/ / [https://meet.librement-votre.fr](https://meet.librement-votre.fr)/ / [https://jitsi.milkywan.fr](https://jitsi.milkywan.fr)/ / [https://jitsi.linux.it](https://jitsi.linux.it) / [https://calls.disroot.org](https://calls.disroot.org) / [https://jitsi.riot.im](https://jitsi.riot.im) / [https://jitsi2.linux.it](https://jitsi2.linux.it) / [https://meet.guifi.net](https://meet.guifi.net) / [https://meet.greenhost.net](https://meet.greenhost.net) / [https://fairmeeting.net](https://fairmeeting.net)/ / [https://meet.artifaille.fr](https://meet.artifaille.fr) / [https://www.colibris-outilslibres.org](https://www.colibris-outilslibres.org)/ (elegir "visio") / [https://meet.collective.tools](https://meet.collective.tools)
[instancia belnet](https://jitsi-1.belnet.be)
- [chat p2p](https://p2p.chat) gratis, sin límite de participantes, en un navegador, sin inicio de sesión / cuenta y totalmente encriptado p2p incluso en llamadas de múltiples participantes. Actualmente sin mantenimiento.
- [Peer Calls](https://peercalls.com/) (sin registro, basado en navegador, FLOSS) funciona muy bien hasta 4 personas
- [Open Meetings](https://openmeetings.apache.org) Apache Open Meetings este es un software para instalar en el servidor; ¿Hay instancias disponibles de esto?
- [Unhangout](https://unhangout.media.mit.edu/about/)
- [ChatB](https://chatb.org/) FOSS videochat sencillo
- [Next Cloud Talk](https://nextcloud.com/talk/) parte de Nextcloud videoconferencia y uso compartido de pantalla (no es un buen rendimiento para grupos > 5, a menos que use el back-end de alto rendimiento, que ahora también ha sido de código abierto, ver [https://nextcloud.com/blog/open-sourcing-talk-back-end-rc-of-talk-9-brings-lots-of-improvements/](https://nextcloud.com/blog/open-sourcing-talk-back-end-rc-of-talk-9-brings-lots-of-improvements/)
-[Big Blue Button](https://bigbluebutton.org/)  una solución completa en el navegador para autohospedaje, pero pocas o ninguna instancia de acceso gratuito: "pregunte a su laboratorio de piratería local" hosts: [https://collocall.de](https://collocall.de) [https://fairteaching.net](https://fairteaching.net)  [https://www.org.meet.coop](https://www.org.meet.coop)
- [Review de Mozilla sobre plataformas de video conferencia](https://foundation.mozilla.org/en/privacynotincluded/categories/video-call-apps) (la mayoría no son libres)
-[Jami](https://jami.net) chats p2p uno a uno, con audio en grupo + videoconferencia: aplicación unificada disponible para varios sistemas operativos
- [Tox](https://tox.chat) p2p chat y audio + videoconferencias: las aplicaciones funcionan mejor en el ámbito del escritorio (ver [qTox](https://qtox.github.io/) )
- Kopano Meet: [https://meet-app.io/](https://meet-app.io/) [https://github.com/Kopano-dev/meet](https://github.com/Kopano-dev/meet) [https://kopano.com/meet-open-source-video-calling/](https://kopano.com/meet-open-source-video-calling/) [https://documentation.kopano.io/kopano_meet_manual/](https://documentation.kopano.io/kopano_meet_manual/)
- Reuniones web multipartitas utilizando mediasoup y WebRTC: [https://github.com/edumeet/edumeet](https://github.com/edumeet/edumeet) [https://hub.docker.com/r/misi/mm/](https://hub.docker.com/r/misi/mm/)  [demo](https://letsmeet.no/)
- [Sylk Suite](https://sylkserver.com/) 
- Un par más listadas [acá](https://wiki.p2pfoundation.net/Free_Code_Chat_Software)

Libre pero sólo con audio:
- [Mumble](https://www.mumble.com/mumble-download.php) 
- Algunos servers: [https://mumble.mayfirst.org](https://mumble.mayfirst.org) / [https://mumble.disroot.org](https://mumble.disroot.org) 

## 2. PROGRAMACION DE REUNIONES GRUPALES (alternativas a doodle)

- [https://dudle.inf.tu-dresden.de/](https://dudle.inf.tu-dresden.de/)
- [https://framadate.org/](https://framadate.org/)
- [https://nuages.domainepublic.net/](https://nuages.domainepublic.net/)
- [https://loomio.org](https://loomio.org) hace polls de agenda también
- [Reuniones simples (como un Calendly auto-hosteado)](https://easyappointments.org/) [GitHub](https://github.com/alextselegidis/easyappointments)

## 3. STREAMING

¿Qué servicios de transmisión de une a muches están disponibles?
- [http://giss.tv/](http://giss.tv/) helen lo probó sin éxito
- [https://icecast.org/](https://icecast.org/) se necesita un servidor para instalarlo
- [https://git.tmp.si/luka/notes/wiki/HOWTO%3A-setup-your-own-Icecast-server](https://git.tmp.si/luka/notes/wiki/HOWTO%3A-setup-your-own-Icecast-server) cómo setear rápidamente tu servidor Icecast
- [https://live.autistici.org](https://live.autistici.org)
- [https://openstreamingplatform.com](https://openstreamingplatform.com) Open Streaming Platform (OSP) es un front-end de software de transmisión RTMP de código abierto para el módulo Nginx-RTMP de Arut. OSP fue diseñado como una alternativa autohospedada a servicios como Twitch.tv, Ustream.tv, Mixer y Youtube Live.
- [https://github.com/mafintosh/hypervision](https://github.com/mafintosh/hypervision) / [https://github.com/louiscenter/hypercast](https://github.com/louiscenter/hypercast) hypervision/ hypercast : Transmisión de video descentralizada sin servidor, basada en el protocolo p2p DAT. Un poco experimental, pero funciona.
- [https://www.crowdcast.io/](https://www.crowdcast.io/) una plataforma de pago por uso. sin descargas, funciona en un navegador, transmisiones integrables.
- [https://www.berlinalive.de](https://www.berlinalive.de) y [https://dringeblieben.de](https://dringeblieben.de) son plataformas donde puede transmitir y las personas pueden donar para su transmisión. (¿Es esto realmente un servidor de transmisión? solicita un "enlace a la transmisión en vivo", por lo que parece más una plataforma para distribuir transmisiones provenientes de otros lugares, en lugar de un servidor de transmisión real en sí mismo; pero no lo he probado y allí no hay mucha información en el sitio)
- [https://voicerepublic.com](https://voicerepublic.com) transmisión de audio, podcasts, ahora también ofrecen transmisión en vivo, dedicada principalmente al discurso / / contenido académico y debate (registro pero gratis)

## 4. VIDEO HOSTING
    
- [https://joinpeertube.org](https://joinpeertube.org) aspira a ser una alternativa descentralizada y libre a los servicios de transmisión de video. (FLOSS y gratuito y federado: la transmisión en vivo está en la hoja de ruta)
- [https://mediagoblin.org/](https://mediagoblin.org/) "MediaGoblin es una plataforma de publicación de medios de software libre que cualquiera puede ejecutar. Puede pensar en ella como una alternativa descentralizada a Flickr, YouTube, SoundCloud, etc."
- [https://archive.org](https://archive.org) Internet Archive, con sede en EE. UU., Es una biblioteca sin fines de lucro de millones de libros, películas, software, música, sitios web gratuitos y más

## 5. CHAT

### Chat federado

Hay muchas organizaciones que albergan servidores a los que puede unirse y cada servidor puede hablar con los demás (como cómo funciona el correo electrónico, gmail, yahoo, hotmail, etc., etc.)

- [Delta chat](https://delta.chat/en/) utiliza el correo electrónico como backend. buena idea
- [MATRIX](https://matrix.org) Red abierta para comunicación libre y segura. Uno de sus clientes: [Riot](https://about.riot.im/) Nueva marca [Element](https://element.io)
- [XMPP](https://xmpp.org/) un protocolo abierto con múltiples implementaciones.
- Scuttlebutt: [https://scuttlebutt.nz/](https://scuttlebutt.nz/) [https://github.com/ssbc](https://github.com/ssbc) [https://en.wikipedia.org/wiki/Secure_Scuttlebutt](https://en.wikipedia.org/wiki/Secure_Scuttlebutt) (aunque quizás deba ir en la categoría de chat p2p abajo?)

### Chat centralizado

Una companía u organización corre el servicio de chat.

- [Wire](https://app.wire.com/) Wire es una aplicación de colaboración y comunicación cifrada disponible para iOS, Android, Windows, macOS, Linux y navegadores web como Firefox. Wire ofrece un paquete de colaboración que incluye mensajería, llamadas de voz, videollamadas, conferencias telefónicas, intercambio de archivos y colaboración externa, todo protegido por un cifrado seguro de extremo a extremo. Gratis para uso personal. Fuente abierta. Necesita crear una cuenta con una dirección de correo electrónico.
IRC (¡ya muy viejo, pero todavía útil y vivo!)
Sin embargo, no es seguro ni privado de fábrica. Se requiere una configuración adicional para garantizar la privacidad de la comunicación.
- [Mattermost](https://mattermost.com/)
- Rocket.Chat: [https://rocket.chat/](https://rocket.chat/) [https://github.com/RocketChat/Rocket.Chat](https://github.com/RocketChat/Rocket.Chat)
- [Telegram Chat](https://web.telegram.org)
- [Signal](https://signal.org/) 

**NOTA:** Tanto Signal como Telegram son de código abierto (GPL), pero Signal es segura desde el primer momento, mientras que Telegram no lo es! El código del servidor de Telegram no es de código abierto

### P2P Chat

Los dispositivos se comunican entre sí sin servidores que transmitan mensajes. Algunos no pueden enviar mensajes a menos que el dispositivo de otras personas esté en línea. Las aplicaciones en dispositivos móviles consumen mucha batería.
- [Cabal](https://cabal.chat) p2p, chat grupal distribuido sin servidor
- [Jami](https://jami.net) chat p2p uno a uno, pero con audio + videoconferencia grupal
- [Tox](https://tox.chat) chat p2p uno a uno, pero con audio + videoconferencia grupal
- [Briar](https://briarproject.org/) chat p2p, sólo Android 

## 6. ETHERPADS

- [https://etherpad.wikimedia.org](https://etherpad.wikimedia.org)
- [https://cryptpad.fr/](https://cryptpad.fr/)
- [https://pad.systemli.org/](https://pad.systemli.org/)
- [https://pad.riseup.net/](https://pad.riseup.net/)
- [https://framapad.org/fr/](https://framapad.org/fr/)
- [https://pad.publiclab.org](https://pad.publiclab.org)
- [https://pad.disroot.org](https://pad.disroot.org)
- [https://pad.kefir.red/etherpad/](https://pad.kefir.red/etherpad/)
- [EtherCalc](https://ethercalc.net/)  spreadsheet en la web
- [cryptpad](https://cryptpad.piratenpartei.de/) también [https://cryptpad.fr](https://cryptpad.fr)
- La red Chatons propone una tonelada de servicios distribuidos, para suavizar el trabajo y la carga del servidor para Framasoft: [https://entraide.chatons.org/en/](https://entraide.chatons.org/en/)

### Markdown Pads (CodiMD)

- [https://demo.codimd.org/](https://demo.codimd.org/)
- [https://escrever.coletivos.org/](https://escrever.coletivos.org/)
- [https://pad.programando.li/](https://pad.programando.li/)

## 7. INTERNET FEMINISTA

- [https://feministinternet.com](https://feministinternet.com)
- [https://feministinternet.org/](https://feministinternet.org/) >> Principios Feministas de Internet

## 8. REDES SOCIALES ALTERNATIVAS

- WeDistribute "una publicación dedicada al software libre, las tecnologías de comunicación descentralizadas y la sostenibilidad" [https://wedistribute.org/](https://wedistribute.org/)
- Bienvenides al Fediverso [https://fediverse.party/](https://fediverse.party/)
- [https://runyourown.social/](https://runyourown.social/) Cómo manejar una red social pequeña con tus amigues 

### Microblogging

- Mastodon: [https://joinmastodon.org/](https://joinmastodon.org/) // [https://instances.social/](https://instances.social/) 
Poseer y ejecutar en colaboración una instancia de mastodonte como parte de una cooperativa [https://social.coop3](https://social.coop3)
- Pleroma [https://pleroma.social/](https://pleroma.social/) // [https://fediverse.party/en/pleroma](https://fediverse.party/en/pleroma)
- Misskey [https://join.misskey.page/en/](https://join.misskey.page/en/) // [https://fediverse.party/en/misskey](https://fediverse.party/en/misskey)

### Macroblogging 

- Socialhome [https://socialhome.network/](https://socialhome.network/) // [https://fediverse.party/en/socialhome](https://fediverse.party/en/socialhome)
- Hubzilla [https://zotlabs.org/page/hubzilla/hubzilla-project](https://zotlabs.org/page/hubzilla/hubzilla-project) // [https://fediverse.party/en/hubzilla](https://fediverse.party/en/hubzilla)
- [Zap](https://zotlabs.com/zap/)
- Diaspora: [https://diasporafoundation.org/](https://diasporafoundation.org/) // [https://fediverse.party/en/diaspora](https://fediverse.party/en/diaspora)
- Friendica:  [https://friendi.ca](https://friendi.ca)  //  [https://fediverse.party/en/friendica](https://fediverse.party/en/friendica)
- [secure scuttlebutt  (SSB)](https://scuttlebutt.nz)
- [manyverse](https://www.manyver.se/) para celus

## 9. COLABORACIÓN ARTÍSTICA

- [UpStage](https://upstage.org.nz)  manipulación colaborativa en tiempo real de medios digitales (imágenes, animaciones, audio, transmisiones en vivo, texto, etc.) junto con un chat de texto, para juegos creativos, improvisaciones, actuaciones y presentaciones; envíe un correo electrónico a info [@] upstage.org.nz si desea iniciar sesión como invitado. estamos llevando a cabo eventos durante la pandemia.
- [PixelJam](https://pixeljam.glitch.me/) consola colaborativa para visuales livecodeadas
- [extramuros](https://github.com/dktr0/extramuros) sistema de codificación en vivo en red de búfer compartido neutro en el idioma
- [Icestream](https://sourceforge.net/projects/icestream/) El mezclador de transmisión de audio permite múltiples transmisiones para un rendimiento de audio distribuido (servidor y cliente)
- [hydra](https://hydra-editor.glitch.me) Visuales livecodeadas en el browser 
- [Estuary](https://estuary.mcmaster.ca/) interfaz online para tocar con TidalCycles en modo solo o colaborativo 
- [Flok](https://flok.clic.cf/)  editor de texto p2p online collaborativo para Hydra y TidalCycles o FoxDot o SuperCollider. [https://flok.clic.cf/  https://github.com/munshkr/flok](https://flok.clic.cf/  https://github.com/munshkr/flok). 
- [Livelabapp](https://livelab.app/) enrutador de medios basado en navegador para un rendimiento colaborativo 

## 10. AUDIO TECH

- enrutamiento de una DAW en transmisión de video a través de @ahihih [https://github.com/ahihi/daw-loopback-guide](https://github.com/ahihi/daw-loopback-guide)
- Music Hackspace @MusicHackspace. Hay nuevo foro! Unánse a la discusión.
- Ubuntu Studio es un sistema operativo gratuito y de código abierto, y una versión oficial de Ubuntu. Ubuntu Studio es el sistema operativo orientado a multimedia más utilizado en el mundo. Viene preinstalado con una selección de las aplicaciones multimedia gratuitas más comunes disponibles y está configurado para obtener el mejor rendimiento para varios propósitos: audio, gráficos, video, fotografía y publicación. [http://ubuntustudio.org](http://ubuntustudio.org) 
- Ruteador de audio de computadoras [https://jackaudio.org](https://jackaudio.org)
- [Audacity](https://www.audacityteam.org/)
- [Manejador de ADSR en samples](https://www.adsrsounds.com/product/software/adsr-sample-manager/)
- [Ardour](https://ardour.org/) un DAW muy potente para audio y midi, Floss, pero no gratuito como en $$
- Reaper: muy buena DAW, no FLOSS pero tiene soporte nativo para Linux, y una interminable versión "Demo" gratuita: [http://reaper.fm/](http://reaper.fm/) (también checkear "Ultraschall", como Reaper pero para podcasting: [https://ultraschall.fm/](https://ultraschall.fm/) )

## 11. DESARROLLO DE VIDEOJUEGOS

- [Twine](http://twinery.org/)
- [Ren'Py](https://www.renpy.org/)
- [Bitsy](http://ledoux.io/bitsy/editor.html)
- [Fabularium (app)](https://fossdroid.com/a/fabularium.html) 
- [http://everest-pipkin.com/teaching/tools.html](http://everest-pipkin.com/teaching/tools.html)
- [Flatpack](https://candle.itch.io/flatpack)
- [Godot](https://godotengine.org/)

## 12. REALIDAD VIRTUAL

- [https://www.relativty.com/](https://www.relativty.com/)

## 13. ALMACENAMIENTO PROPIO (con otres)

- [Freedombox](https://freedomboxfoundation.org/)
- [Freedombone](https://freedombone.net/)
- [Yunohost](https://yunohost.org/)
- Guías prácticas y más [https://homebrewserver.club/](https://homebrewserver.club/) más links:[https://pad.vvvvvvaria.org/self-hosting-together](https://pad.vvvvvvaria.org/self-hosting-together)
- Lista de servidores feministas: [https://pad.riseup.net/p/femservers-checklist-security](https://pad.riseup.net/p/femservers-checklist-security )
- Cómo montar una servidora feminista con una conexión casera [https://labekka.red/novedades/2019/11/05/lanzamiento-fanzine.html](https://labekka.red/novedades/2019/11/05/lanzamiento-fanzine.html)
- [Diglife](https://diglife.com) co-operativa de usuaries desarrollando herramientas de internet  
- Otros proovedores de servicios de internet co-operativos a los que podemos unirnos: [https://libreho.st](https://libreho.st)

## 14. TELÉFONOS CELULARES

- [https://mastodon.bida.im/@hollow/104070793178741385](https://mastodon.bida.im/@hollow/104070793178741385) gráfico que muestra versiones alternativas (que no sean de Google, etc.) para todo en teléfonos móviles; lo muestra usando el sistema operativo Lineage, sin embargo, las herramientas sugeridas también funcionan en otros sistemas operativos de teléfonos inteligentes.
Aplicaciones de mensajería, vinculadas a su número de teléfono (es posible utilizar un número de teléfono diferente al que tenga acceso, incluso una línea fija)
Signal & Telegram para mensajería (para evitar Whatsapp): ¿alguno de estos es de código abierto?
Signal y telegram son ambos de código abierto (GPL), pero Signal es seguro desde el primer momento, mientras que Telegram no lo es. !!!! El código del servidor de telegramas no es de código abierto, el código fuente del servidor de publicación de Signal
Aplicaciones de mensajería que no están vinculadas a un número de teléfono: consulte Wire, Element y XMPP en la sección de mensajería anterior.
- Tienda de aplicaciones alternativa para teléfonos de Google solo tiene aplicaciones de código abierto: [F-Droid](https://f-droid.org/)
- Aplicaciones diarias: Antennapod (podcatcher), Fennec-F-droid (browser), OsmAnd (navegación), AuoraStore (Aplicaciones de Play Store anónimas: puede usarlo para obtener Collabora, la mejor suite ofimática de código abierto para ver y editar hojas de cálculo, documentos, presentaciones, etc.), Gallery, Draw, Calculator, Torchie, Camera Roll, NewPipe (youtube anonimizado), Librera Pro (ereader), Scarlet Notes, Tusky (fediverse client)...
- [https://bromite.org](https://bromite.org) tienen su propio repositorio fdroid. (agréguelo en la configuración de la aplicación fdroid). Un gran navegador para la privacidad y la seguridad.
- [El repo de izzyondroid](https://apt.izzysoft.de/fdroid/) tiene más apps de código abierto 

### Teléfonos celulares en sí

- [Pinephone](https://www.pine64.org/pinephone/) teléfono en desarrollo por Linux
- [Fairphone](https://www.fairphone.com/en/)más sostenible y reparable posible) instala Android 9 por defecto, pero propone cómo rootear el teléfono e instalar otro sistema en sus [páginas de soporte](https://support.fairphone.com/hc/en-us/articles/360032971751-Operating-systems-OS-for-the-Fairphone-3)
- [e.foundation](https://e.foundation/) Instale el software usted mismx o compre uno reacondicionado con ellxs (incluye un teléfono inteligente eliminado de Google) / e / está bifurcado de LineageOS (Android sin Google)
- [Librem by Purism](https://puri.sm/products/librem-5/with)  PureOS, no Andoid 
- [Puede ser interesante tener un poco de contexto](https://fosdem.org/2020/schedule/event/smartphones/)
- [CalyxOS](https://calyxinstitute.org/about/board) Sistema operativo de código abierto basado en Android centrado en la privacidad y la seguridad, al tiempo que mantiene una buena funcionalidad desarrollada por el Instituto Calyx, disponible para teléfonos Google Pixel y Xiaomi Mi A2. [https://calyxos.org](https://calyxos.org)
- [GrapheneOS](https://grapheneos.org) sistema operativo de código abierto basado en Android enfocado en brindar alta seguridad y privacidad
- [https://www.blloc.com/](https://www.blloc.com/)

## 15. PARA NIÑES

- [https://pad.constantvzw.org/p/Ketjes](https://pad.constantvzw.org/p/Ketjes)
- [Framinetest Edu (a Minetest server)](https://framinetest.org/ )
- [Framagames](https://framagames.org/ )

## 16. SEGURIDAD EN FOTOS

- [IMAGE SCRUBBER](https://everestpipkin.github.io/image-scrubber/) datos de desenfoque rápido y partes no deseadas de una fotografía por seguridad

## 17. LINKS ÚTILES

- [https://switching.software/](https://switching.software/) "Alternativas éticas, fáciles de usar y conscientes de la privacidad al software conocido"
- [Aral explains in video presi](https://small-tech.org/videos/creative-mornings-istanbul/)
- [https://beyond-social.org/wiki/index.php/Social_Practices_COVID-19_Teaching_Resources](https://beyond-social.org/wiki/index.php/Social_Practices_COVID-19_Teaching_Resources)
- [https://prism-break.org/de/all/](https://prism-break.org/de/all/)
- [https://www.are.na/melanie-hoff/teaching-online](https://www.are.na/melanie-hoff/teaching-online)
-[https://wiki.p2pfoundation.net/World_of_Free_and_Open_Source_Art](https://wiki.p2pfoundation.net/World_of_Free_and_Open_Source_Art)
- [Coronavirus Tech Handbook ](https://coronavirustechhandbook.com/)
- [https://syllabus.pirate.care/topic/coronanotes/](https://syllabus.pirate.care/topic/coronanotes/) Pirate Care: aplanar la curva, hacer crecer la atención: ¿Qué estamos aprendiendo de Covid-19?
- [https://detroitcommunitytech.org/](https://detroitcommunitytech.org/)  /  [https://detroitcommunitytech.org/?q=teachcommtech](https://detroitcommunitytech.org/?q=teachcommtech) - Detroit Community Technology Project
- [https://alliedmedia.org/consentful-tech-project](https://alliedmedia.org/consentful-tech-project)  Consentful Tech Project
- [https://ps.zoethical.org/c/engagement/prendre-soin/137](https://ps.zoethical.org/c/engagement/prendre-soin/137) prendre soin
- [https://pad.xpub.nl/p/MEDIA-onlineArt_and_resources](https://pad.xpub.nl/p/MEDIA-onlineArt_and_resources) HKU Image and Media Technology; Recursos culturales online de reciente apertura: museos, bibliotecas, streaming de óperas, etc.
- [https://www.systemli.org/en/2020/03/15/solidarity-as-infrastructure.html](https://www.systemli.org/en/2020/03/15/solidarity-as-infrastructure.html)  "Como colectivo tecnológico de izquierda, queremos apoyarlo en el uso de las herramientas adecuadas sin dejar de mantener un mínimo de estándares de privacidad y seguridad. A continuación se presentan algunas recomendaciones específicas de herramientas que consideramos útiles para los procesos de trabajo colaborativo digital".
- [https://dat.foundation/explore/projects/](https://dat.foundation/explore/projects/)  DAT es un protocolo para compartir datos entre computadoras y es la base de muchos proyectos interesantes de implementación de igual a igual, como [Wireline](https://www.wireline.io/) y [CoBox](https://cobox.cloud/)
- [https://www.filmsforaction.org/](https://www.filmsforaction.org/)  un centro de intercambio de ideas, conocimientos y perspectivas esenciales para crear un mundo más justo, sostenible y hermoso.
- [www.defectivebydesign.org](www.defectivebydesign.org) una guía para la gestión de derechos digitales medios gratuitos: libros, vídeo, audio y software
- [https://www.defectivebydesign.org/blog/staying_safe_while_you_stream_dbds_tips_living_drmfree_during_quarantine](https://www.defectivebydesign.org/blog/staying_safe_while_you_stream_dbds_tips_living_drmfree_during_quarantine)
- [https://framalibre.org/](https://framalibre.org/) directorio de software FLOSS, en francés
- [https://degooglisons-internet.org/](https://degooglisons-internet.org/) (degoogleando internet)
- [Disroot Covid19kit](https://disroot.org/en/covid19) (usando algunas de las herramientas de arriba) 
- [Riseup.net lista de servidores radicales](https://riseup.net/en/security/resources/radical-servers)
- [May First Movement Technology](https://mayfirst.coop/en/)
- [https://www.fsf.org/blogs/community/better-than-zoom-try-these-free-software-tools-for-staying-in-touch](https://www.fsf.org/blogs/community/better-than-zoom-try-these-free-software-tools-for-staying-in-touch)
- [Seguridad en las protestas  &  seguridad digitaldirigida a personas trans que protestan contra el racismo contra los negros y la brutalidad policial en los EE. UU.](https://github.com/trans-hues/trans-resist)
- [Partager c'est aimer](https://wiki.mur.at/SharingIsCaring)

## 18. COMPARTIR DOCUMENTOS

- [NextCloud](https://nextcloud.com/) Uso compartido de documentos de código abierto, alternativa para Dropbox, Google Docs, etc. Debe estar alojado en un servidor. Hay algunos proveedores, puede registrarse gratis [aquí](https://nextcloud.com/signup/) (hay bastantes opciones, pero deberá verificar cada empresa individualmente para saber qué tan buenas pueden ser) o verificar con su servidor web, por ejemplo Mur.at proporciona NextCloud para sus miembrxs.
- [Cryptpad](https://disroot.org/en/services/cryptpad) (registración gratuita)
- [https://thegood.cloud/](https://thegood.cloud/) usa NextCloud, para individuxs es gratis hasta 2GB
- [CoBox](https://cobox.cloud/) CoBox es una nube cooperativa distribuida, primero fuera de línea, basada en DAT
- [Gitlab](Gitlab.com) → uso compartido de archivos de control de versiones basado en git
- [Seafile](https://www.seafile.com/en/home/) código abierto, sincronización y uso compartido de archivos autohospedados.
- [OnlyOffice](https://github.com/ONLYOFFICE/DocumentServer) Editores de documentos, diapositivas y hojas de cálculo autohospedados y de escritorio.

## 19. REDES DE SOLIDARIDAD DIGITAL

- [https://pad.vvvvvvaria.org/digital-solidarity-networks](https://pad.vvvvvvaria.org/digital-solidarity-networks)
- [https://www.digitaldefenders.org/](https://www.digitaldefenders.org/) The Digital Defenders Partnership ofrece apoyo a los defensores de los derechos humanos bajo amenaza digital y trabaja para fortalecer las redes locales de respuesta rápida.

## 20. ARTÍCULOS

- [PURISM Our Essential List of Free Software for Remote Work March 25, 2020 Software FLOSS applications / Software freedom / Tips and tricks](https://puri.sm/posts/our-essential-list-of-free-software-for-remote-work/)
- [What do feminists have to say on #COVID19. A thread on Resources from (mostly) Global South thinkers.](https://threadreaderapp.com/thread/1244225040507379712.html)
- [The Politics of COVID-19, Readings #14The most important contributions on the political, economic, and social effects of the unfolding crisis.](https://covid19syllabus.substack.com/p/the-politics-of-covid-19-readings-471)

## 21. EMAIL

- [https://opensource.com/alternatives/gmail](https://opensource.com/alternatives/gmail)
- [https://mur.at](https://mur.at) Solo en alemán. proveedor de código abierto gratuito que también ofrece servicios de nube y correo gratuitos y seguros
- [https://libreplanet.org/wiki/Email](https://libreplanet.org/wiki/Email)
- [https://switching.software/replace/gmail/](https://switching.software/replace/gmail/)

## 22. DINERO

Pregunta: ¿Existen alternativas interesantes para el micropago como ko-fi que no está conectado a paypal / stripe y tampoco un desastre ambiental (btc)?

???

## 23. OTROS/ SIN CATEGORÍA

- [Una plataforma educativa de comunicación federada](https://olki.loria.fr/platform/)
- [Aprendizaje a distancia](https://www.are.na/melanie-hoff/teaching-online)
- [https://left.gallery/](https://left.gallery/)
- [https://todays.supply/](https://todays.supply/)
- [EDICIÓN DE VIDEO: DaVinci Resolve (editor de video profesional, no es un software de código abierto pero tiene una política de usuario amigable, soporte para Linux y una versión totalmente gratuita)](https://www.blackmagicdesign.com/products/davinciresolve/)  --> [Cómo instalar Resolve en Linux](https://www.fosslinux.com/24381/how-to-install-davinci-resolve-on-ubuntu.htm)
- [https://libreho.st](https://libreho.st) una red de diferentes proveedores de servicios web comunitarios / comunes / cooperativos, muchos de los cuales ofrecen los servicios enumerados 